package Facilities;

import FacilityMaintenance.IFacilityMaintenance;
import FacilityUse.IFacilityUse;

/**
 * Created by dalestoutjr on 2/4/15.
 */
public interface IUnit {

    /* ******* */
    /* SETTERS */
    /* ******* */

    public void setId(int unitId);

    /* ******* */
    /* GETTERS */
    /* ******* */

    public int getId();

}
