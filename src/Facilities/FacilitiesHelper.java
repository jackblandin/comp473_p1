package Facilities;

import DB.FacilitiesHelperDAO;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by dalestoutjr on 2/4/15.
 * Manages Buildings
 */
public class FacilitiesHelper {

    private FacilitiesHelperDAO facilitiesHelperDAO = new FacilitiesHelperDAO();

    /* **************** */
    /* BUILDING METHODS */
    /* **************** */

    public List<Building> getAllBuildings() {
        List<Building> bList = facilitiesHelperDAO.getAllBuildings();
        return bList;
    }
    public Building getBuilding(int id) {
        return facilitiesHelperDAO.getBuilding(id);
    }
    public String addBuilding(Building b){
        String resultMessage = facilitiesHelperDAO.addBuilding(b);
        return resultMessage;
    }
    public void removeBuilding(int id){
        // TODO
    }

    /* ************* */
    /* FLOOR METHODS */
    /* ************* */

    public List<Floor> getAllFloors(int buildingId) {
        return facilitiesHelperDAO.getAllFloors(buildingId);
    }
    public Floor getFloor(int id) {
        return facilitiesHelperDAO.getFloor(id);
    }
    public void addFloor(Floor f){
        // TODO
    }
    public void removeFloor(int id){
        // TODO
    }
    /* ************ */
    /* UNIT METHODS */
    /* ************ */

    public List<Unit> getAllUnits(int floorId) {
        return facilitiesHelperDAO.getAllUnits(floorId);
    }
    public Unit getUnit(int id) {
        return facilitiesHelperDAO.getUnit(id);
    }
    public void addUnit(Unit unit){
        // TODO
    }

    public void removeUnit(int id){
        // TODO
    }


//
//
//
//    /**
//     * METHOD
//     * @param building
//     * @param floor
//     * @return
//     */
//    public boolean addNewUnit(Building building, Floor floor){
//        int index = buildingList.indexOf(building);
//        IBuilding helperBuilding = buildingList.get(index);
//        index = helperBuilding.getFloorList().indexOf(floor);
//        IFloor helperFloor = (Floor)helperBuilding.getFloorList().get(index);
//        IUnit helperUnit = new Unit();
//        return helperFloor.getUnitsOnFloorList().add(helperUnit);
//    }
//
//    /**
//     * METHOD
//     * @param building
//     * @param floor
//     * @param unit
//     * @return
//     */
//    public boolean removeUnit(Building building, Floor floor, Unit unit){
//        int index = buildingList.indexOf(building);
//        IBuilding helperBuilding = buildingList.get(index);
//        index = helperBuilding.getFloorList().indexOf(floor);
//        IFloor helperFloor = (Floor)helperBuilding.getFloorList().remove(index);
//        return helperFloor.getUnitsOnFloorList().remove(unit);
//    }
//
//    /**
//     * METHOD
//     * @return void
//     */
//    public void listBuildings(){
//        Iterator iterator = buildingList.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next().toString());
//        }
//        return;
//    }
//
//    /**
//     * METHOD
//     * @param building
//     */
//    public void listFloors(Building building){
//        int index = buildingList.indexOf(building);
//        IBuilding helperBuilding = buildingList.get(index);
//        Iterator iterator = helperBuilding.getFloorList().iterator();
//        while(iterator.hasNext()){
//            System.out.println(iterator.next().toString());
//        }
//    }
//
//    /**
//     * METHOD
//     * @param building
//     * @param floor
//     */
//    public void listUnits(Building building, Floor floor){
//        int index = buildingList.indexOf(building);
//        IBuilding helperBuilding = buildingList.get(index);
//        index = helperBuilding.getFloorList().indexOf(floor);
//        IFloor helperFloor = (Floor)helperBuilding.getFloorList().remove(index);
//        Iterator iterator = helperFloor.getUnitsOnFloorList().iterator();
//        while(iterator.hasNext()){
//            System.out.println(iterator.next().toString());
//        }
//
//    }
//
//
//    /**
//     *
//     * @param buildingId
//     * @param floorId
//     * @return IFloor
//     */
//    public IFloor returnFloor(int buildingId, int floorId){
//        for (int i = 0; i < buildingList.size(); i++){
//            if(buildingList.get(i).getBuildingId() == buildingId){
//                for (int j = 0; j < buildingList.get(i).getFloorList().size(); j++){
//                    IFloor tempFloor = (Floor) buildingList.get(i).getFloorList().get(j);
//                    if(tempFloor.getFloorId() == floorId){
//                        return (IFloor)buildingList.get(i).getFloorList().get(j);
//                    }
//
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     *
//     * @param buildingId
//     * @param floorId
//     * @param unitId
//     * @return IUnit
//     */
//    public IUnit returnUnit(int buildingId, int floorId, int unitId){
//        for (int i = 0; i < buildingList.size(); i++){
//            if(buildingList.get(i).getBuildingId() == buildingId){
//                for (int j = 0; j < buildingList.get(i).getFloorList().size(); j++){
//                    IFloor tempFloor = (Floor) buildingList.get(i).getFloorList().get(j);
//                    if(tempFloor.getFloorId() == floorId){
//                        for(int k = 0; k < tempFloor.getUnitsOnFloorList().size(); k++){
//                            IUnit tempUnit = (Unit)tempFloor.getUnitsOnFloorList().get(k);
//                            if(tempUnit.getId() == unitId){
//                                return (IUnit) ((Floor) buildingList.get(i).getFloorList().get(j)).getUnitsOnFloorList().get(k);
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     * METHOD
//     * @param buildingId
//     */
//    public void listActualUsage(int buildingId){
//        for (int i = 0; i < buildingList.size(); i++){
//            if(buildingList.get(i).getBuildingId() == buildingId){
//                for (int j = 0; j < buildingList.get(i).getFloorList().size(); j++){
//                    IFloor tempFloor = (Floor) buildingList.get(i).getFloorList().get(j);
//                    for(int k = 0; k < tempFloor.getUnitsOnFloorList().size(); k++){
//                        IUnit tempUnit = (Unit)tempFloor.getUnitsOnFloorList().get(k);
//                        for(int l = 0; l < tempUnit.getFacilityUseHelper().getFacilityUseList().size(); l++){
//                            IFacilityUse tempFacilityUse = tempUnit.getFacilityUseHelper().getFacilityUseList().get(l);
//                            if (tempFacilityUse.getInUse()) {
//                                System.out.println(tempUnit.toString());
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//    }
//
//    /**
//     * METHOD
//     * @param buildingId
//     */
//    public void calcUsageRate(int buildingId){
//        double inUse = 0;
//        double total = 0;
//        for (int i = 0; i < buildingList.size(); i++) {
//            if (buildingList.get(i).getBuildingId() == buildingId) {
//                for (int j = 0; j < buildingList.get(i).getFloorList().size(); j++) {
//                    IFloor tempFloor = (Floor) buildingList.get(i).getFloorList().get(j);
//                    for (int k = 0; k < tempFloor.getUnitsOnFloorList().size(); k++) {
//                        IUnit tempUnit = (Unit) tempFloor.getUnitsOnFloorList().get(k);
//                        for (int l = 0; l < tempUnit.getFacilityUseHelper().getFacilityUseList().size(); l++) {
//                            IFacilityUse tempFacilityUse = tempUnit.getFacilityUseHelper().getFacilityUseList().get(l);
//                            if (tempFacilityUse.getInUse()) {
//                                inUse++;
//                                total++;
//                            } else {
//                                total++;
//                            }
//                        }
//
//                    }
//                }
//            }
//        }
//        System.out.println(inUse/total);
//    }
//
//    public void calcProblemRateForFacility(int buildingId){ // NOT COMPLETED METHOD
//        double problems = 0;
//        double total = 0;
//        for (int i = 0; i < buildingList.size(); i++){
//            if(buildingList.get(i).getBuildingId() == buildingId){
//                for (int j = 0; j < buildingList.get(i).getFloorList().size(); j++){
//                    IFloor tempFloor = (Floor) buildingList.get(i).getFloorList().get(j);
//                    for(int k = 0; k < tempFloor.getUnitsOnFloorList().size(); k++){
//                        IUnit tempUnit = (Unit)tempFloor.getUnitsOnFloorList().get(k);
//                        if(tempUnit.getFacilityMaintenance().getMaintenanceLogList().size() > 0){
//                            problems++;
//                            total++;
//                        }
//                        else{
//                            total++;
//                        }
//                    }
//
//                }
//            }
//        }
//        System.out.println(problems/total);
//    }






}
