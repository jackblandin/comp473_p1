package Facilities;

import java.util.ArrayList;

/**
 * Created by dalestoutjr on 2/1/15.
 */

public interface IBuilding {

    /* ******* */
    /* SETTERS */
    /* ******* */

    public void setBuildingId(int Id);
    public void setBuildingName(String buildingName);
    public void setBuildingAddress(String buildingAddress);
    public void setBuildingCapacity(int buildingCapacity);
    public void setBuildingDetail(String buildingDetail);
    public void setBuildingInformation(String buildingInformation);

    /* ******* */
    /* GETTERS */
    /* ******* */

    public int getBuildingId();
    public String getBuildingName();
    public String getBuildingAddress();
    public int getBuildingCapacity();
    public String getBuildingInformation();
    public String getBuildingDetail();
    public int requestAvailableCapacity();
    public ArrayList getFloorList();

}
