package Facilities;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by jackblandin on 2/9/15.
 */

public class Facility {

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private Building building;
    private String name;
    private String detail;

    /* ************ */
    /* CONSTRUCTORS */
    /* ************ */

    public Facility() {}

    /* ******* */
    /* GETTERS */
    /* ******* */

    public Building getBuilding() { return building; }
    public String getName() { return name; }
    public String getDetail() {return detail; }

    /* ******* */
    /* SETTERS */
    /* ******* */

    public void setBuilding(Building _building) { building = _building; }
    public void setName(String _name) { name = _name; }
    public void setDetail(String _detail) { detail = _detail; }

}
