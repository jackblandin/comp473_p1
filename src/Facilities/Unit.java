package Facilities;

import FacilityMaintenance.IFacilityMaintenance;
import FacilityMaintenance.FacilityMaintenance;
import FacilityUse.IFacilityUse;
import FacilityUse.FacilityUse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalestoutjr on 2/1/15.
 */
public class Unit implements IUnit {

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private int id;
    private int floorId;
    private int number;
    private IFacilityUse useHelper;
    private IFacilityMaintenance facilityMaintenance;

    /* ************ */
    /* CONSTRUCTORS */
    /* ************ */

    public Unit() {}

    public Unit(int id){
        this.id = id;
        this.facilityMaintenance = new FacilityMaintenance();
    }

    /* *************** */
    /* GETTERS/SETTERS */
    /* *************** */

    public void setId(int id){this.id = id;}
    public void setFacilityMaintenance(IFacilityMaintenance facilityMaintenance) {this.facilityMaintenance = facilityMaintenance;}
    public int getId(){return this.id;}
    public IFacilityMaintenance getFacilityMaintenance() {return facilityMaintenance;}
    public int getUnitNumber() {
        return number;
    }

    public void setUnitNumber(int number) {
        this.number = number;
    }
    public int getFloorId() {
        return floorId;
    }

    public void setFloorId(int floorId) {
        this.floorId = floorId;
    }

    /* **************** */
    /* HELPER FUNCTIONS */
    /* **************** */

    /**
     * returns a list of field types
     * @return  list of field types
     */
    public static List<String> getFieldTypes() {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("int");
        fieldTypes.add("int");
        return fieldTypes;
    }

    /**
     * used in testing to compare if two units are equal
     * @param   u   unit being compared to (this) unit
     * @return      true or false
     */
    public boolean unitEquals(Unit u) {
        if (u == null) return false;
        if (u == this) return true;
        if (u.getId() != id) {
            return false;
        } else if(u.getFloorId() != floorId) {
            return false;
        } else if(u.getUnitNumber() != number) {
            return false;
        }  else {
            return true;
        }
    }
}