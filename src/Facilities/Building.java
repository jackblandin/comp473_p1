package Facilities;


//import package.FacilityMaintenance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalestoutjr on 2/1/15.
 */
public class Building implements IBuilding {

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private int buildingId;
    private String buildingName;
    private String buildingAddress;
    private int buildingCapacity;
    private String buildingDetail;
    private String buildingInformation;
    private ArrayList<IFloor> buildingFloorList;

    /* ************ */
    /* CONSTRUCTORS */
    /* ************ */

    /**
     * default constructor
     */
    public Building() {
    }

    /**
     * Full Constructor
     * @param buildingName name of building
     * @param buildingAddress building's street address
     * @param facilityCapacity
     * @param facilityDetail
     * @param buildingInformation
     */
    public Building(String buildingName, String buildingAddress, int facilityCapacity, String facilityDetail, String buildingInformation){
        this.buildingName = buildingName;
        this.buildingAddress = buildingAddress;
        this.buildingCapacity = facilityCapacity;
        this.buildingDetail = facilityDetail;
        this.buildingInformation = buildingInformation;
        this.buildingFloorList = new ArrayList<IFloor>();


    }

    /* ************************* */
    /* INTERFACE SETTERS/GETTERS */
    /* ************************* */

    public void setBuildingId(int Id){
        this.buildingId = Id;
    }
    public void setBuildingName(String buildingName){this.buildingName = buildingName;}
    public void setBuildingAddress(String buildingAddress){
        this.buildingAddress = buildingAddress;
    }
    public void setBuildingCapacity(int buildingCapacity){this.buildingCapacity = buildingCapacity;}
    public void setBuildingDetail(String buildingDetail){this.buildingDetail = buildingDetail;}
    public void setBuildingInformation(String buildingInformation){this.buildingInformation = buildingInformation;}
    public int getBuildingId(){
        return this.buildingId;
    }
    public String getBuildingName(){
        return this.buildingName;
    }
    public String getBuildingAddress(){
        return this.buildingAddress;
    }
    public int getBuildingCapacity(){
        return this.buildingCapacity;
    }
    public String getBuildingInformation(){
        return this.buildingInformation;
    }
    public String getBuildingDetail(){
        return this.buildingDetail;
    }
    public int requestAvailableCapacity(){
        return buildingCapacity;
    }
    public ArrayList getFloorList(){
        return this.buildingFloorList;
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */

    /**
     * Gets all the building information as a String
     * @return      all building info
     */
    public String getAllInfo() {
        StringBuilder info = new StringBuilder();
        info.append("id: " + getBuildingId());
        info.append("\nname: " + getBuildingName());
        info.append("\naddress: " + getBuildingAddress());
        info.append("\ncapacity: " + getBuildingCapacity());
        info.append("\ndetail: " + getBuildingDetail());
        return info.toString();
    }
    /**
     * Gets a list of the names of the fields
     * @return      list of building field names
     */
    public static List<String> getFieldNames() {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("name");
        fieldNames.add("address");
        fieldNames.add("capacity");
        fieldNames.add("detail");
        return fieldNames;
    }
    /**
     * Gets a list of Building field types
     * @return      list of building field types
     */
    public static List<String> getFieldTypes() {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("String");
        fieldTypes.add("String");
        fieldTypes.add("int");
        fieldTypes.add("String");
        return fieldTypes;
    }

    /**
     * used in testing
     * @param   b   building being compared
     * @return      true or false
     */
    public boolean buildingEquals(Building b) {
        if (b == null) return false;
        if (b == this) return true;
        if (b.getBuildingId() != buildingId) {
            return false;
        } else if(!b.getBuildingName().equals(buildingName)) {
            return false;
        } else if(!b.getBuildingAddress().equals(buildingAddress)) {
            return false;
        } else if(b.getBuildingCapacity() != buildingCapacity) {
            return false;
        } else if(!b.getBuildingDetail().equals(buildingDetail)) {
            return false;
        } else {
            return true;
        }
    }

}
