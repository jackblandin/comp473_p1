package Facilities;

import java.util.ArrayList;

/**
 * Created by dalestoutjr on 2/4/15.
 */
public interface IFloor {

    /* ******* */
    /* SETTERS */
    /* ******* */

    public void setFloorLevel(int floorLevel);

    /* ******* */
    /* GETTERS */
    /* ******* */

    public int getFloorId();
    public int getFloorLevel();
    public int getNumberUnitsOnFloor();
    public ArrayList getUnitsOnFloorList();
}
