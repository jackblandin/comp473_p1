package Facilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalestoutjr on 2/4/15.
 */
public class Floor implements IFloor {

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private int floorId;
    private int buildingId;
    private int floorLevel;
    private ArrayList<IUnit> unitsOnFloorList;

    /* ************ */
    /* CONSTRUCTORS */
    /* ************ */

    /**
     * EMPTY CONSTRUCTOR
     */
    public Floor() {
    }

    /**
     * FULL CONSTRUCTOR
     * @param floorLevel which level (story) the floor is on
     * @param numberUnitsOnFloor number of units on floor
     */
    public Floor(int floorLevel, int numberUnitsOnFloor){
        this.floorLevel = floorLevel;
        this.unitsOnFloorList = new ArrayList<IUnit>(numberUnitsOnFloor);
    }

    /* ******* */
    /* SETTERS */
    /* ******* */

    /**
     * SETTER
     * @param floorId
     */
    public void setFloorId(int floorId){
        this.floorId = floorId;
    }
    /**
     * SETTER
     * @param floorLevel which level(story) the floor is on
     */
    public void setFloorLevel(int floorLevel){
        this.floorLevel = floorLevel;
    }

    /* ******* */
    /* GETTERS */
    /* ******* */

    /**
     * GETTER
     * @return unique floorId
     */
    public int getFloorId(){
        return this.floorId;
    }
    /**
     * GETTER
     * @return which level(story) this Floor is on
     */
    public int getFloorLevel(){
        return this.floorLevel;
    }

    /**
     * GETTER
     * @return the number of units that are on this floor
     */
    public int getNumberUnitsOnFloor(){
        return unitsOnFloorList.size();
    }

    /**
     * GETTER
     * @return a list of all units on this floor
     */
    public ArrayList getUnitsOnFloorList(){
        return this.unitsOnFloorList;
    }


    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    /* **************** */
    /* HELPER FUNCTIONS */
    /* **************** */
    /**
     * returns a list of field types
     * @return      list of field types
     */
    public static List<String> getFieldTypes() {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("int");
        fieldTypes.add("int");
        return fieldTypes;
    }

    /**
     * used in testing to compare if two floors are equal
     * @param   f   floor being compared to (this) floor
     * @return      true or false
     */
    public boolean floorEquals(Floor f) {
        if (f == null) return false;
        if (f == this) return true;
        if (f.getFloorId() != floorId) {
            return false;
        } else if(f.getBuildingId() != buildingId) {
            return false;
        } else if(f.getFloorLevel() != floorLevel) {
            return false;
        }  else {
            return true;
        }
    }


}
