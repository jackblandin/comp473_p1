package main;

import Facilities.Building;
import Facilities.FacilitiesHelper;
import FacilityMaintenance.FacilityMaintenance;
import FacilityMaintenance.MaintenanceHelper;
import FacilityMaintenance.MaintenanceRequest;
import FacilityUse.FacilityUse;
import FacilityUse.IUseRequest;
import FacilityUse.UseHelper;
import FacilityUse.UseRequest;
import Tests.Test;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jackblandin on 2/11/15.
 */
public class Main {
    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static FacilitiesHelper facilitiesHelper = new FacilitiesHelper();
    public static MaintenanceHelper maintenanceHelper = new MaintenanceHelper();
    public static UseHelper useHelper = new UseHelper();

    public static void main(String[] a) throws Exception {


//        Test.runAllTests();

        Boolean showMenu = true;
        while (true) {
            if (showMenu) {
                printMenu();
            }
            showMenu = false;
            System.out.print("\nENTER COMMAND: ");
            String command = reader.readLine();

            /** Menu commands **/
            if (command.equals("menu")) {
                printMenu();
            }

            /** Facility commands **/
            else if (command.equals("f1")) {
                listFacilities();
            } else if (command.equals("f2")) {
                int id = getId("Building");
                getFacilityInformation(id);
            } else if (command.equals("f3")) {
                int id = getId("Building");
                requestAvailableCapacity(id);
            } else if(command.equals("f4")) {
                addNewFacility();
            } else if(command.equals("f5")) {

            } else if(command.equals("f6")) {

            }

            /** FacilityUse commands **/
            else if(command.equals("u1")) {

            } else if(command.equals("u2")) {
                assignUnitToUse();
            } else if(command.equals("u3")) {
                vacateUnit();
            } else if(command.equals("u4")) {
                int id = getId("Facility");
                getAllUseRequests(id, false);
            } else if(command.equals("u5")) {
                int id = getId("Facility");
                getAllUseRequests(id, true);
            } else if(command.equals("u6")) {
                calculateUsageRate();
            }

            /** FacilityMaintenance commands **/
            else if(command.equals("m1")) {
                addMaintenanceRequest();
            } else if(command.equals("m2")) {
                scheduleMaintenance();
            } else if(command.equals("m3")) {

            } else if(command.equals("m4")) {

            } else if(command.equals("m5")) {

            } else if(command.equals("m6")) {
                getAllMaintenanceRequests();
            } else if(command.equals("m7")) {
                int facilityId = getId("facility");
                getAllMaintenance(facilityId);
            } else if(command.equals("m8")) {

            }

            else {
                System.out.println("Command '" + command + "' not found.");
            }
        }
    }

    /* ************************** */
    /* facility command functions */
    /* ************************** */

    public static void listFacilities() {
        List<Building> buildingList = new ArrayList<Building>();
        buildingList = facilitiesHelper.getAllBuildings();
        for (Building b : buildingList) {
            System.out.println("\n" + b.getAllInfo());
        }
    }

    public static void getFacilityInformation(int id) {
        Building building = facilitiesHelper.getBuilding(id);
        System.out.println("\n" + building.getAllInfo());
    }

    public static void requestAvailableCapacity(int id) {
        Building building = facilitiesHelper.getBuilding(id);
        int capacity = building.getBuildingCapacity();
        System.out.println("\nCapacity: " + capacity);
    }

    public static void addNewFacility() throws Exception{
        Building newBuilding = new Building();
        System.out.print("ENTER NAME: ");
        newBuilding.setBuildingName(reader.readLine());
        System.out.print("ENTER ADDRESS: ");
        newBuilding.setBuildingAddress(reader.readLine());
        System.out.print("ENTER CAPACITY");
        newBuilding.setBuildingCapacity(Integer.parseInt(reader.readLine()));
        System.out.print("ENTER DETAIL: ");
        newBuilding.setBuildingDetail(reader.readLine());
        facilitiesHelper.addBuilding(newBuilding);
    }

    /* ************************************* */
    /* use request command functions */
    /* ************************************* */

    public static void assignUnitToUse() throws Exception{
        System.out.print("ENTER UNIT ID: ");
        int facilityId = Integer.parseInt(reader.readLine());
        System.out.print("ENTER USE REQUEST ID: ");
        int urId = Integer.parseInt(reader.readLine());
        FacilityUse fUse = new FacilityUse();
        fUse.setFacilityId(facilityId);
        fUse.setCurrentUseRequest(urId);
    }
    public static void vacateUnit() throws Exception {
        int unitId = getId("unit");
        useHelper.vacateUnit(unitId);
    }
    public static void getAllUseRequests(int facilityId, boolean getOnlyUsed) {
        List<IUseRequest> urList = useHelper.getAllUseRequests(facilityId, getOnlyUsed);
        for (IUseRequest ur: urList) {
            System.out.println("\n" + ur.getAllInfo());
        }
    }
    public static void calculateUsageRate() throws Exception{
        int unitId = getId("unit");
        double usageRate = useHelper.calculateUsageRate(unitId);
        System.out.println("\nusage rate: $" + usageRate);
    }

    /* ************************************* */
    /* maintenance request command functions */
    /* ************************************* */

    public static void addMaintenanceRequest() throws Exception {
        MaintenanceRequest mr = new MaintenanceRequest();
        mr.setDateRequested(getDate("date from"));
        mr.setDateCompleted(getDate("date to"));
        mr.setName(getString("name"));
        mr.setDescription(getString("description"));
        mr.setScope(getDouble("project scope"));
        mr.setUnitsId(getId("unit"));

        maintenanceHelper.addMaintenanceRequest(mr);

    }
    public static void scheduleMaintenance() throws Exception {
        int mrId = getId("Maintenance Request");
        Date dateStart = getDate("start date");
        Date dateEnd = getDate("date end");
        maintenanceHelper.scheduleMaintenanceRequest(mrId, dateStart, dateEnd);
    }
    public static void getAllMaintenanceRequests() {
        List<MaintenanceRequest> maintenanceRequestList = maintenanceHelper.getAllMaintenanceRequests();
        for (MaintenanceRequest mr: maintenanceRequestList) {
            System.out.println("\n" + mr.getAllInfo());
        }
    }

    public static void getAllMaintenance(int facilityId) {
        List<MaintenanceRequest> mList = maintenanceHelper.getAllMaintenance(facilityId);
        for (MaintenanceRequest mr: mList) {
            System.out.println("\n" + mr.getAllInfo());
        }
    }

    /* ********************** */
    /* menu command functions */
    /* ********************** */

    public static void printMenu() {
        String menu =
            "\nFacility" +
            "\nf1: listFacilities()" +
            "\nf2: getFacilityInformation()" +
            "\nf3: requestAvailableCapacity()" +
            "\nf4: addNewFacility()" +
            "\nf5: addFacilityDetail()" +
            "\nf6: removeFacility()" +
            "\n\nFacility Use" +
            "\nu1: isInUseDuringInterval()" +
            "\nu2: assignFacilityToUse()" +
            "\nu3: setFacility()" +
            "\nu4: listInspections()" +
            "\nu5: listActualUsage()" +
            "\nu6: calcUsageRate()" +
            "\n\nFacility Maintenance" +
            "\nm1: makeFacilityMaintRequest()" +
            "\nm2: scheduleMaintenance()" +
            "\nm3: calcMaintenanceCostForFacility()" +
            "\nm4: calcProblemRateForFacility()" +
            "\nm5: calcDownTimeForFacility()" +
            "\nm6: listMaintRequests()" +
            "\nm7: listMaintenance()" +
            "\nm8: listFacilityProblems()" +
            "\n\ntype 'menu' to re-display menu commands\n";
        System.out.println(menu);
    }



    /** Helper Functions **/

    public static int getId(String objectName) throws Exception {
        System.out.print("ENTER " + objectName.toUpperCase() + " ID: ");
        int id = Integer.parseInt(reader.readLine());
        return id;
    }

    public static String getString(String name) throws Exception {
        System.out.print("ENTER " + name.toUpperCase() + ": ");
        String string = reader.readLine();
        return string;
    }

    public static double getDouble(String name) throws Exception {
        System.out.print("ENTER " + name.toUpperCase() + ": ");
        String string = reader.readLine();
        double dbl = Double.parseDouble(string);
        return dbl;
    }

    public static Date getDate(String name) throws Exception {
        System.out.print("ENTER " + name.toUpperCase() + " (yyyy-mm-dd): ");
        String string = reader.readLine();
        Date date = stringToDate(string);
        return date;
    }

    public static boolean getBoolean(String name) throws Exception {
        System.out.print("ENTER " + name.toUpperCase() + "('Y' FOR YES, 'N' FOR NO): ");
        String string = reader.readLine().toLowerCase();
        boolean bool = false;
        if(string == "y") {
            bool = true;
        }
        return bool;
    }

    public static Date stringToDate(String s) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        LocalDate localDate = LocalDate.parse(s, formatter);
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return date;
    }
}
