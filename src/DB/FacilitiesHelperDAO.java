package DB;

import Facilities.Building;
import Facilities.Floor;
import Facilities.Unit;
import Tests.Facilities.UnitTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jackblandin on 2/11/15.
 */
public class FacilitiesHelperDAO {

    /* ******************** */
    /* BUILDING DAO METHODS */
    /* ******************** */

    /**
     * Gets all buildings in our database.
     * @return
     */
    public List<Building> getAllBuildings() {

        // Get SELECT String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String>  constraints = new ArrayList<String>();
        String select = DBHelper.SELECTQueryString(fields, "tbl_Buildings", constraints, "", innerJoins);

        // get fieldTypes
        List<String> fieldTypes = Building.getFieldTypes();

        // execute query
        Object buildingObject = DBHelper.executeQuery(select, fieldTypes);

        // cast object to a List<Building>
        List<Building> bList = llObjectToBuildingList(buildingObject);

        return bList;

    }

    /**
     * Gets a building (from db_facilities) by its Id
     * @param   Id      building Id
     * @return          building with Id = Id
     */
    public Building getBuilding(int Id) {

        Building b = new Building();

        // Get SELECT query String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String> constraints = new ArrayList<String>();
        constraints.add("id = " + Id);
        String select = DBHelper.SELECTQueryString(fields, "tbl_Buildings", constraints, "", innerJoins);

        // execute query
        Object buildingObject = DBHelper.executeQuery(select, Building.getFieldTypes());

        // convert buildingObject into an actual Building
        b = llObjectToBuilding((List<ArrayList<Object>>) buildingObject);
        return b;
    }

    public String addBuilding(Building b) {
        // Get INSERT String
        List<String> fieldNames = Building.getFieldNames();
        List<String> values = new ArrayList<String>();
        values.add("'" + b.getBuildingName() + "'");
        values.add("'" + b.getBuildingAddress() + "'");
        values.add(Integer.toString(b.getBuildingCapacity()));
        values.add("'" + b.getBuildingDetail() + "'");
        String insert = DBHelper.INSERTQueryString("tbl_buildings", fieldNames, values);
        System.out.println("\nsuccess !\n");

        // execute non-query
        String resultMessage = DBHelper.executeNonQuery(insert);
        return resultMessage;
    }
    public void removeBuilding(int id) {
        // TODO
    }

    /* ***************** */
    /* FLOOR DAO METHODS */
    /* ***************** */

    /**
     * Gets all Floors for a given Building
     * @param buildingId    id of Building where we want to get the Floors from
     * @return              list of all Floors in the Building
     */
    public List<Floor> getAllFloors(int buildingId) {
        // Get SELECT String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String>  constraints = new ArrayList<String>();
        constraints.add("building_Id = " + buildingId);
        String select = DBHelper.SELECTQueryString(fields, "tbl_floors", constraints, "", innerJoins);

        // get fieldTypes
        List<String> fieldTypes = Floor.getFieldTypes();

        // execute query
        Object floorObject = DBHelper.executeQuery(select, fieldTypes);

        // cast object to a List<Floor>
        List<Floor> fList = llObjectToFloorList(floorObject);
        return fList;
    }
    public Floor getFloor(int Id) {
        Floor f = new Floor();

        // Get SELECT query String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String> constraints = new ArrayList<String>();
        constraints.add("id = " + Id);
        String select = DBHelper.SELECTQueryString(fields, "tbl_floors", constraints, "", innerJoins);

        // execute query
        Object floorObject = DBHelper.executeQuery(select, Floor.getFieldTypes());

        // convert buildingObject into an actual Building
        f = llObjectToFloor((List<ArrayList<Object>>) floorObject);
        return f;
    }
    public void addFloor(Floor f) {
        // TODO
    }
    public void removeFloor(int id) {
        // TODO
    }

    /* **************** */
    /* UNIT DAO METHODS */
    /* **************** */

    public List<Unit> getAllUnits(int floorId) {
        // TODO
        return new ArrayList<Unit>();
    }

    public Unit getUnit(int id) {
        Unit u = new Unit();

        // Get SELECT query String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String> constraints = new ArrayList<String>();
        constraints.add("id = " + id);
        String select = DBHelper.SELECTQueryString(fields, "tbl_units", constraints, "", innerJoins);

        // execute query
        Object unitObject = DBHelper.executeQuery(select, Unit.getFieldTypes());

        // convert unitObject into an actual Unit
        u = llObjectToUnit((List<ArrayList<Object>>) unitObject);

        return u;

    }
    public void addUnit(Unit u) {
        // TODO
    }
    public void removeUnit(int id) {
        // TODO
    }

    /**
     * Executes the actual SQL query.
     * This is where we communicate with the database.
     * @param
     * @return
     */
//    public List<Building> executeQuery(String statement) {
//        List<Building> bList = new ArrayList<Building>();
//        List<String> fieldTypes = Building.getFieldTypes();
//        Object o = DBHelper.executeQuery(statement, fieldTypes);
//        return objectToBuilding(o);
//        return bList;
//    }

    public List<Building> llObjectToBuildingList(Object object) {

        // cast object to list of Objects
        List<Object> oList = (List<Object>)object;

        // cast List<Object> to List<Buildings>
        List<Building> bList= new ArrayList<Building>();
        for(Object o: oList) {
            Building b = lObjectToBuilding((List<Object>)o);
            bList.add(b);
        }
        return bList;
    }

    public Building llObjectToBuilding(Object object) {

        // convert object to a List<Building>
        List<Building> bList = llObjectToBuildingList(object);

        // take out the Building in the list (should only be one)
        Building b = bList.get(0);

        return b;
    }


    public Building lObjectToBuilding(List<Object> lObject) {
        Building b = new Building();
        int id = (Integer)lObject.get(0);
        String name = ((String)lObject.get(1)).trim();
        String address = ((String)lObject.get(2)).trim();
        int capacity = (Integer)lObject.get(3);
        String detail = ((String)lObject.get(4)).trim();

        b.setBuildingId(id);
        b.setBuildingName(name);
        b.setBuildingAddress(address);
        b.setBuildingCapacity(capacity);
        b.setBuildingDetail(detail);
        return b;
    }

    public List<Floor> llObjectToFloorList(Object object) {

        // cast object to list of Objects
        List<Object> oList = (List<Object>)object;

        // cast List<Object> to List<Floor>
        List<Floor> fList = new ArrayList<Floor>();
        for(Object o: oList) {
            Floor f = lObjectToFloor((List<Object>)o);
            fList.add(f);
        }
        return fList;
    }

    public Floor llObjectToFloor(Object object) {

        // convert object to a List<Floor>
        List<Floor> fList = llObjectToFloorList(object);

        // take out the Floor in the list (should only be one)
        Floor f = fList.get(0);

        return f;
    }


    public Floor lObjectToFloor(List<Object> lObject) {
        Floor f = new Floor();
        int id = (Integer)lObject.get(0);
        int builidingId = ((Integer)lObject.get(1));
        int level = ((Integer)lObject.get(2));

        f.setFloorId(id);
        f.setBuildingId(builidingId);
        f.setFloorLevel(level);

        return f;
    }

    public List<Unit> llObjectToUnitList(Object object) {

        // cast object to list of Objects
        List<Object> oList = (List<Object>)object;

        // cast List<Object> to List<Floor>
        List<Unit> uList = new ArrayList<Unit>();
        for(Object o: oList) {
            Unit u = lObjectToUnit((List<Object>)o);
            uList.add(u);
        }
        return uList;
    }

    public Unit llObjectToUnit(Object object) {

        // convert object to a List<Unit>
        List<Unit> uList = llObjectToUnitList(object);

        // take out the Unit in the list (should only be one)
        Unit u = uList.get(0);

        return u;
    }

    public Unit lObjectToUnit(List<Object> lObject) {

        Unit u = new Unit();
        int id = (Integer)lObject.get(0);
        int floorId = (Integer)lObject.get(1);
        int number = ((Integer)lObject.get(2));

        u.setId(id);
        u.setFloorId(floorId);
        u.setUnitNumber(number);

        return u;


    }
}

