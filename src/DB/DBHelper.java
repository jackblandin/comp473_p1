package DB;

import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jackblandin on 2/9/15.
 */
public class DBHelper {

    /**
     * Constructs the SELECT query string to be used in our Statement.
     * @param fields        list of fields(columns) to be returned (SELECT)
     * @param tableName     name of db table (FROM)
     * @param constraints   constraints (WHERE)
     * @param orderByField  sort field (ORDER BY)
     * @return              constructed query string
     */
    public static String SELECTQueryString(List<String> fields, String tableName, List<String> constraints, String orderByField, List<String> innerJoins) {

        // Construct the SELECT clause
        String fieldString = "";
        int fieldCount = fields.size();
        String all = "*";
        if (fields.get(0) == "*") {
            fieldString = "*";
        } else {
            for(int i = 0; i < fieldCount - 1; i++) {
                String f = fields.get(i);
                fieldString = fieldString + ", ";
            }
            fieldString = fieldString + fields.get(fieldCount - 1);
        }

        // add in the INNER JOIN statements
        String innerJoinString = "";
        for (int i = 0; i < innerJoins.size(); i++) {
            innerJoinString = innerJoinString + innerJoins.get(i) + " ";
        }
        // Construct the WHERE clause(s)
        int constraintCount = constraints.size();
        String constraintString = "WHERE ";
        if (constraintCount == 0) {
            constraintString = "";
        } else {
            for (int i = 0; i < constraintCount - 1; i++) {
                constraintString = constraintString + " AND " + constraints.get(i);
            }
            constraintString = constraintString + constraints.get(constraintCount - 1);
        }

        // Construct the ORDER BY clause
        String orderByString = "";
        if (orderByField.length() > 0) {
            orderByString = "ORDER BY " + orderByField;
        }
        String select = "SELECT " + fieldString + " FROM " + tableName + " " + innerJoinString + " " + constraintString + " " + orderByString;
        return select.trim();

    }

    /**
     * creates an INSERT query string to be used in a Statement
     * @param tblName   name of table we are inserting into
     * @param fields    list of column names we are inserting values for
     * @param values    list of values we are inserting
     * @return          the completed insert query string
     */
    public static String INSERTQueryString(String tblName, List<String> fields, List<String> values) {
        StringBuilder insert = new StringBuilder();

        insert.append("INSERT INTO " + tblName + " (");

        // insert fields
        int fieldCount = fields.size();
        for(int i = 0; i < fieldCount - 1; i++) {
            String f = fields.get(i);
            insert.append(f + ", ");
        }
        insert.append(fields.get(fieldCount - 1));
        insert.append(") ");

        // insert values
        insert.append("VALUES (");
        int valueCount = values.size();
        for(int i = 0; i < valueCount - 1; i++) {
            String v = values.get(i);
            insert.append(v + ", ");
        }
        insert.append(values.get(valueCount - 1));
        insert.append(") ");

        return insert.toString().trim();
    }

    /**
     * gets a string for an UPDATE nonQuery statement
     * @param tblName       name of table
     * @param sets       list of updates
     * @param constraints   list of constraints
     * @return              UPDATE statement as a string
     */
    public static String getUPDATE(String tblName, List<String> sets, List<String> constraints) {

        StringBuilder update = new StringBuilder();
        update.append("UPDATE " + tblName);

        // construct sets
        StringBuilder set = new StringBuilder();
        set.append(" SET ");
        for(int i = 0; i < sets.size() - 1; i++) {
            String u = sets.get(i);
            set.append(u + ", ");
        }
        set.append(sets.get(sets.size() - 1));
        set.append(" ");

        // Construct the WHERE clause(s)
        StringBuilder constraint = new StringBuilder();
        constraint.append("WHERE ");
        int constraintCount = constraints.size();
        if (constraints.size() == 0) {
            constraint = new StringBuilder();
        } else {
            for (int i = 0; i < constraints.size() - 1; i++) {
                constraint.append(constraints.get(i) + " AND ");
            }
            constraint.append(constraints.get(constraintCount - 1));
        }

        update.append(set.toString());
        update.append(constraint.toString());
        return update.toString().trim();

    }

    /**
     * Query execution with database.
     * @param statement
     * @param fieldTypes
     * @return
     */
    public static Object executeQuery(String statement, List<String> fieldTypes) {
        List<Object> objectList = new ArrayList<Object>();
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(statement);
            int fieldCount = fieldTypes.size();
            while ( rs.next() ) {
                List<Object> fieldValues = new ArrayList<Object>();
                for(int i = 1; i < fieldCount + 1; i++) {
                    String ft = fieldTypes.get(i - 1);
                    if (ft == "String") {
                        fieldValues.add(rs.getString(i));
                    } else if (ft == "int") {
                        fieldValues.add(rs.getInt(i));
                    } else if (ft == "double") {
                        fieldValues.add(rs.getDouble(i));
                    } else if (ft == "decimal") {
//                        String decimalString = rs.getBigDecimal(i).toString();
                        String decimalString = rs.getString(i);
                        double decimalDouble = Double.parseDouble(decimalString);
                        fieldValues.add(decimalDouble);
                    } else if (ft == "Date") {
                        fieldValues.add(rs.getDate(i));
                    } else if (ft =="boolean") {
                        fieldValues.add(rs.getBoolean(i));
                    }
                }
                objectList.add(fieldValues);
            }
            rs.close();
            st.close();
            return objectList;

        } catch (SQLException sqlEx) {
            System.err.println("");
            System.err.println(sqlEx.getMessage());
            sqlEx.printStackTrace();
            System.out.println("attempted sql statement: " + statement);
        }
        return null;
    }
    public static String executeNonQuery(String statement) {
        try {
            Statement st = getConnection().createStatement();
            st.execute(statement);
            System.out.println("\nSUCCESS!\n");
            return "success";

        } catch (SQLException sqlEx) {
            System.err.println(sqlEx.getMessage());
            System.out.println("attempted sql statement: " + statement);
            return sqlEx.getMessage();
        }
    }

    public static Connection getConnection() {

//        System.out.println("DBHelper: -------- PostgreSQL " + "JDBC Connection  ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("DBHelper: Check Where  your PostgreSQL JDBC Driver exist and " + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

//        System.out.println("DBHelper: PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/jackblandin", "jackblandin", "");

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT VERSION()");

            if (rs.next()) {
//                System.out.println("DBHelper: The Database Version is " + rs.getString(1));
            }

        } catch (SQLException e) {

            System.out.println("DBHelper: Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

        if (connection != null) {
//            System.out.println("DBHelper: You have a database connection!");
        } else {
            System.out.println("DBHelper: Failed to make connection!");
        }

        return connection;
    }
}
