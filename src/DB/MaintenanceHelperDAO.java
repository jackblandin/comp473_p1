package DB;

import FacilityMaintenance.FacilityMaintenance;
import FacilityMaintenance.MaintenanceHelper;
import FacilityMaintenance.MaintenanceRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by jackblandin on 2/15/15.
 */


public class MaintenanceHelperDAO {

    /* **************************** */
    /* MaintenanceRequest functions */
    /* **************************** */

    public List<MaintenanceRequest> getAllMaintenanceRequests() {

        // Get SELECT STRING
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String> constraints = new ArrayList<String>();
        String select = DBHelper.SELECTQueryString(fields, "tbl_maintenancerequests", constraints, "", innerJoins);

        // get fieldTypes
        List<String> fieldtypes = MaintenanceRequest.getFieldTypes();

        // execute query
        Object maintenanceRequestObject = DBHelper.executeQuery(select, fieldtypes);

        // transform object to a List<MaintenanceRequest>
        List<MaintenanceRequest> maintenanceRequestList = llObjectToMaintenanceRequestList(maintenanceRequestObject);
        return maintenanceRequestList;
    }

    public List<MaintenanceRequest> getAllMaintenance(int facilityId) {
        // Get SELECT String
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        List<String> constraints = new ArrayList<String>();
        constraints.add("id = " + facilityId);
        String select = DBHelper.SELECTQueryString(fields, "tbl_maintenancerequests", constraints, "", innerJoins);

        // get fieldTypes
        List<String> fieldTypes = MaintenanceRequest.getFieldTypes();

        // execute query
        Object maintenanceObject = DBHelper.executeQuery(select, fieldTypes);

        // transform object to a List<MaintenanceRequest>
        List<MaintenanceRequest> mrList = llObjectToMaintenanceRequestList(maintenanceObject);
        return mrList;
    }

    public void addMaintenanceRequest(MaintenanceRequest mr) {

        // get INSERT statement string
        List<String> fields = MaintenanceRequest.getFieldNames();
        List<String> values = new ArrayList<String>();
        values.add(dateToPostgreDate(mr.getDateCompleted()));
        values.add(dateToPostgreDate(mr.getDateRequested()));
        values.add("'" + mr.getName() + "'");
        values.add("'" + mr.getDescription() + "'");
        values.add(Double.toString(mr.getScope()));
        values.add(Integer.toString(mr.getUnitsId()));

        String insert = DBHelper.INSERTQueryString("tbl_maintenancerequests", fields, values);

        // execute nonQuery
        DBHelper.executeNonQuery(insert);

    }

    public void scheduleMaintenanceRequest(int maintenanceRequestId, Date dateStart, Date dateEnd) {

        // get UPDATE statement
        List<String> sets = new ArrayList<String>();
        String dStart = dateToPostgreDate(dateStart);
        String dEnd = dateToPostgreDate(dateEnd);
        sets.add("datestart = " + dStart);
        sets.add("dateend = " + dEnd);
        List<String> constraints = new ArrayList<String>();
        constraints.add("id = " + maintenanceRequestId);
        String update = DBHelper.getUPDATE("tbl_maintenancerequests", sets, constraints);

        // execute UPDATE
        DBHelper.executeNonQuery(update);
    }


    /* HELPER FUNCTIONS */


    public static String dateToPostgreDate(Date date){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        cal.setTime(date);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-mm-dd");
        String formatted = format1.format(cal.getTime());
        String toDateFormat = "to_date('" + formatted + "', 'YYYY-MM-DD')";
        return toDateFormat;

    }

    public List<MaintenanceRequest> llObjectToMaintenanceRequestList(Object object) {
        // cast object to a List<Object>
        List<Object> objectList = (List<Object>)object;

        // transform List<Object> to List<MaintenanceRequest>
        List<MaintenanceRequest> maintenanceRequestList = new ArrayList<MaintenanceRequest>();
        for (Object o: objectList) {
            MaintenanceRequest mr = lObjectToMaintenanceRequest((List<Object>)o);
            maintenanceRequestList.add(mr);
        }
        return maintenanceRequestList;
    }

    public MaintenanceRequest llObjectToMaintenanceRequest(Object object) {

        // transform object to a List<MaintenanceRequest>
        List<MaintenanceRequest> mrList = llObjectToMaintenanceRequestList(object);

        // take out the Maintenance Request in the list (should only be one)
        MaintenanceRequest mr = mrList.get(0);

        return mr;
    }

    public MaintenanceRequest lObjectToMaintenanceRequest(List<Object> lObject) {
        MaintenanceRequest mr = new MaintenanceRequest();
        int id = (Integer)lObject.get(0);
        Date dateTo = (Date)lObject.get(1);
        Date dateFrom = (Date)lObject.get(2);
        String name = lObject.get(3).toString();
        String description = lObject.get(4).toString();
        double scope = Double.parseDouble(lObject.get(5).toString());
        int unitsId = (Integer)lObject.get(6);
        Date dateStart = (Date)lObject.get(7);
        Date dateEnd = (Date)lObject.get(8);

        mr.setId(id);
        mr.setDateRequested(dateFrom);
        mr.setDateCompleted(dateTo);
        mr.setName(name);
        mr.setDescription(description);
        mr.setScope(scope);
        mr.setUnitsId(unitsId);
        mr.setDateStart(dateStart);
        mr.setDateEnd(dateEnd);

        return mr;
    }
}
