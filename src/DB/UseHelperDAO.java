package DB;

import FacilityUse.IUseRequest;
import FacilityUse.UseRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jackblandin on 2/16/15.
 */
public class UseHelperDAO {
    public List<IUseRequest> getAllUseRequests(int facilityId, boolean getOnlyUsed) {

        // get SELECT string
        List<String> fields = new ArrayList<String>();
        fields.add("*");
        List<String> innerJoins = new ArrayList<String>();
        innerJoins.add("INNER JOIN tbl_units ON tbl_userequests.units_id = tbl_units.id");
        innerJoins.add("INNER JOIN tbl_floors ON tbl_units.floors_id = tbl_floors.id");
        innerJoins.add("INNER JOIN tbl_buildings ON tbl_floors.building_id = tbl_buildings.id");
        List<String> constraints = new ArrayList<String>();
        constraints.add("building_id = " + facilityId);
        if (getOnlyUsed) {
            constraints.add("isUsed = TRUE");
        }
        String select = DBHelper.SELECTQueryString(fields, "tbl_userequests", constraints, "", innerJoins);

        // get fieldTypes
        List<String> fieldTypes = UseRequest.getFieldTypes();

        // execute query
        Object useRequestObject = DBHelper.executeQuery(select, fieldTypes);

        // transform object to a List<UseRequest>
        List<IUseRequest> useRequestList = llObjectToUseRequestList(useRequestObject);

        return useRequestList;
    }

    public void setCurrentUseRequest(int useRequestId, int facilityId) {

        // get (old useRequests) UPDATE string
        List<String> oldSets = new ArrayList<String>();
        oldSets.add("dateto = NOW()");

        // for each useRequest in tbl_useRequests for this facility,
        //      if isUsed == true AND ( dateTo == NULL OR dateTo > (NOW) )
        //          change dateTo to (NOW)
        List<String> oldConstraints = new ArrayList<String>();
        oldConstraints.add("isused = true");
        oldConstraints.add("(dateto = NULL OR dateto > NOW())");
        oldConstraints.add("units_id = " + facilityId);

        String oldUpdateString = DBHelper.getUPDATE("tbl_userequests", oldSets, oldConstraints);

        // get new userequest UPDATE string
        List<String> newSets = new ArrayList<String>();
        newSets.add("isused = true");
        List<String> newConstraints = new ArrayList<String>();
        newConstraints.add("id = " + useRequestId);

        String newUpdateString = DBHelper.getUPDATE("tbl_userequests", newSets, newConstraints);

        // execute UPDATE nonQuery
        DBHelper.executeNonQuery(oldUpdateString);
        DBHelper.executeNonQuery(newUpdateString);
    }

    public void vacateUnit(int unitId) {

        // get UPDATE string
        List<String> sets = new ArrayList<String>();
        sets.add("dateto = NOW()");
        List<String> constraints = new ArrayList<String>();
        constraints.add("units_id = " + unitId);
        constraints.add("isused = true");
        constraints.add("dateto = (SELECT MAX(dateto) FROM tbl_userequests WHERE isused = true AND units_id = " + unitId + ")");
        String updateString = DBHelper.getUPDATE("tbl_userequests", sets, constraints);

        // execute UPDATE nonQuery
        DBHelper.executeNonQuery(updateString);
    }

    public double calculateUsageRate(int unitId) {

        // get SUM SELECT string
        List<String> all = new ArrayList<String>();
        all.add("*");
        List<String> constraints = new ArrayList<String>();
        constraints.add("units_id = " + unitId);
        String selectString = DBHelper.SELECTQueryString(all, "tbl_userequests", constraints, "", new ArrayList<String>());
        List<String> fieldTypes = UseRequest.getFieldTypes();
        Object object = DBHelper.executeQuery(selectString, fieldTypes);
        List<IUseRequest> uList = llObjectToUseRequestList(object);
        double totalCost = 0;
        int useCount = 0;
        for (IUseRequest iur: uList) {
            totalCost = totalCost + iur.getRent();
            useCount++;
        }
        double usageRate = totalCost / useCount;
        // rounds to 2 decimal places
        usageRate = (double)Math.round(usageRate * 100) / 100;
        return usageRate;

    }
    /* **************** */
    /* HELPER FUNCTIONS */
    /* **************** */

    public List<IUseRequest> llObjectToUseRequestList(Object object) {

        // cast object to a List<Object>
        List<Object> objectList = (List<Object>)object;

        // transform List<Object to List<UseRequest>
        List<IUseRequest> urList = new ArrayList<IUseRequest>();
        for (Object o: objectList) {
            UseRequest ur = lObjectToMaintenanceRequest((List<Object>)o);
            urList.add(ur);
        }
        return urList;
    }

    public IUseRequest llObjectToUseRequest(Object object) {

        // transform object to a List<UseRequest>
        List<IUseRequest> urList = llObjectToUseRequestList(object);

        // take out the UseRequest in the list (should only be one)
        IUseRequest ur = urList.get(0);

        return ur;
    }

    public UseRequest lObjectToMaintenanceRequest(List<Object> lObject) {
        UseRequest ur = new UseRequest();
        int id = (Integer)lObject.get(0);
        int unitId = (Integer)lObject.get(1);
        Date dateFrom = (Date)lObject.get(2);
        Date dateTo = (Date)lObject.get(3);
        String tenant = lObject.get(4).toString();
        double rent = Double.parseDouble(lObject.get(5).toString());
        String inspection = lObject.get(6).toString();
        String description = lObject.get(7).toString();
        boolean isUsed = (Boolean)lObject.get(8);

        ur.setId(id);
        ur.setUnitId(unitId);
        ur.setDateFrom(dateFrom);
        ur.setUsed(dateTo);
        ur.setTenant(tenant);
        ur.setRent(rent);
        ur.setInspection(inspection);
        ur.setDescription(description);
        ur.setIsUsed(isUsed);

        return ur;
    }
}

