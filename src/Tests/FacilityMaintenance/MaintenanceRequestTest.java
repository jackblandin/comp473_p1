package Tests.FacilityMaintenance;

import FacilityMaintenance.MaintenanceRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class MaintenanceRequestTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetId() throws Exception {

    }

    @Test
    public void testSetId() throws Exception {

    }

    @Test
    public void testGetUnitsId() throws Exception {

    }

    @Test
    public void testSetUnitsId() throws Exception {

    }

    @Test
    public void testGetName() throws Exception {

    }

    @Test
    public void testSetName() throws Exception {

    }

    @Test
    public void testIsComplete() throws Exception {

    }

    @Test
    public void testSetComplete() throws Exception {

    }

    @Test
    public void testSetScope() throws Exception {

    }

    /* HELPER FUNCTIONS */

    public static List<MaintenanceRequest> getQuickList() {
        List<MaintenanceRequest> mrList = new ArrayList<MaintenanceRequest>();
        mrList.add(getQuick1());
        mrList.add(getQuick2());
        return mrList;
    }
    public static MaintenanceRequest getQuick2() {
        MaintenanceRequest mr = new MaintenanceRequest();
        mr.setId(1);
        Calendar cal = Calendar.getInstance();
        cal.set(2015, Calendar.FEBRUARY, 15);
        Calendar cal2 = Calendar.getInstance();
        cal2.set(2015, Calendar.FEBRUARY, 15);
        mr.setDateCompleted(cal2.getTime());
        mr.setDateRequested(cal.getTime());
        mr.setDescription("mop all floors");
        mr.setName("clean floors");
        mr.setScope(100);
        mr.setUnitsId(1);
        return mr;
    }

    public static MaintenanceRequest getQuick1() {
        MaintenanceRequest mr = new MaintenanceRequest();
        mr.setId(3);
        Calendar cal = Calendar.getInstance();
        cal.set(2014, Calendar.FEBRUARY, 20);
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2015, Calendar.FEBRUARY, 10);
        mr.setDateCompleted(cal1.getTime());
        mr.setDateRequested(cal.getTime());
        mr.setDescription("clean bathroom floor");
        mr.setName("clean bathroom tile");
        mr.setScope(200);
        mr.setUnitsId(2);
        return mr;
    }


}