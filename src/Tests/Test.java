package Tests;

import Tests.DB.DBHelperTest;
import Tests.DB.FacilitiesHelperDAOTest;
import Tests.Facilities.BuildingTest;
import Tests.Facilities.FacilitiesHelperTest;
import Tests.Facilities.FloorTest;
import Tests.Facilities.UnitTest;
import Tests.FacilityUse.UseRequestTest;

/**
 * Created by jackblandin on 2/9/15.
 */
public class Test {
    /**
     * Runs all tests for this project
     * @throws Exception
     */
    public static void runAllTests() throws Exception {
        DBHelperTest dbHelperTest = new DBHelperTest();
        FacilitiesHelperDAOTest facilitiesHelperDAOTest = new FacilitiesHelperDAOTest();
        FacilitiesHelperTest facilitiesHelperTest = new FacilitiesHelperTest();
        BuildingTest buildingTest = new BuildingTest();
        FloorTest floorTest = new FloorTest();
        UnitTest unitTest = new UnitTest();
        UseRequestTest facilityUseTest = new UseRequestTest();

        dbHelperTest.testAll();
        facilitiesHelperDAOTest.testAll();
        buildingTest.testAll();
        facilitiesHelperTest.testAll();
        floorTest.testAll();
        unitTest.testAll();
        facilityUseTest.testAll();

    }
}
