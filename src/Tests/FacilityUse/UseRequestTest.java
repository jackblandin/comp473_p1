package Tests.FacilityUse;

import FacilityMaintenance.MaintenanceRequest;
import FacilityUse.UseRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class UseRequestTest {

    /**
     * runs all tests in FacilityUseTest
     * @throws Exception
     */
    public void testAll() throws Exception {
        testSetDateFrom();
        testSetDateTo();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFacilityUse() throws Exception {

    }

    @Test
    public void testSetDateFrom() throws Exception {
        UseRequest useRequest = getQuickFacilityUse();
        Calendar cal0 = Calendar.getInstance();
        cal0.set(1969, Calendar.JANUARY, Calendar.MONDAY, 0, 0);
        Date then = cal0.getTime();
//        assertEquals(then, facilityUse.getDateFrom());
    }

    @Test
    public void testSetDateTo() throws Exception {
        UseRequest useRequest = getQuickFacilityUse();
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2015, Calendar.FEBRUARY, Calendar.THURSDAY, 19, 16);
        Date now = cal1.getTime();
        boolean calEq = MaintenanceRequest.calendarDayEquals(cal1.getTime(), useRequest.getDateTo());
        assertTrue(calEq);
    }

    @Test
    public void testSetInUse() throws Exception {

    }

    @Test
    public void testSetInspection() throws Exception {

    }

    @Test
    public void testIsInUseDuringInterval() throws Exception {

    }

    @Test
    public void testVacateFacility() throws Exception {

    }

    /**
     * Helper Function
     * quickly sets all facility use fields
     */
    public UseRequest getQuickFacilityUse() {
        Calendar cal0 = Calendar.getInstance();
        cal0.set(1969, Calendar.JANUARY, Calendar.MONDAY, 0, 0);
        Date then = cal0.getTime();
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2015, Calendar.FEBRUARY, Calendar.THURSDAY, 19, 16);
        Date now = cal1.getTime();
        UseRequest fUse = new UseRequest();
        fUse.setDateFrom(then);
        fUse.setUsed(now);
        fUse.setInspection("JHC inspection");
        fUse.setIsUsed(true);

        return fUse;
    }
}