package Tests.Facilities;

import Facilities.Building;
import Facilities.Floor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FloorTest {

    /**
     * Runs all tests in FloorTest
     * @throws Exception
     */
    public void testAll() throws Exception {
        testGetFieldTypes();
    }


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSetFloorLevel() throws Exception {

    }

    @Test
    public void testGetFloorLevel() throws Exception {

    }

    @Test
    public void testGetNumberUnitsOnFloor() throws Exception {

    }

    @Test
    public void testGetUnitsOnFloorList() throws Exception {

    }

    /* ********************* */
    /* HELPER FUNCTION TESTS */
    /* ********************* */

    /**
     * Test that a floor returns a list of its field types
     * @throws Exception
     */
    @Test
    public void testGetFieldTypes() throws Exception {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("int");
        fieldTypes.add("int");
        assertEquals(fieldTypes, Floor.getFieldTypes());
    }

    /**
     * helper function used for quickly creating a floor
     * @return
     */
    public static Floor getQuickFloor1(){
        Floor floor = new Floor();
        floor.setBuildingId(1);
        floor.setFloorId(1);
        floor.setFloorLevel(1);
        return floor;
    }
    /**
     * helper function used for quickly creating a floor
     * @return
     */
    public static Floor getQuickFloor2(){
        Floor floor = new Floor();
        floor.setBuildingId(1);
        floor.setFloorId(2);
        floor.setFloorLevel(2);
        return floor;
    }
    public static List<Floor> getQuickFloorList() {
        List<Floor> floorList = new ArrayList<Floor>();
        floorList.add(getQuickFloor1());
        floorList.add(getQuickFloor2());
        return floorList;
    }
}