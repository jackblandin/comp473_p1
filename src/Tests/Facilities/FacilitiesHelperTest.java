package Tests.Facilities;

import Facilities.Building;
import Facilities.FacilitiesHelper;
import Facilities.Floor;
import Facilities.Unit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.util.List;

import static org.junit.Assert.*;

public class FacilitiesHelperTest {

    public FacilitiesHelper facilitiesHelper = new FacilitiesHelper();
    @Rule
    public ErrorCollector collector = new ErrorCollector();

    /**
     * Runs all tests in FacilitiesHelperTest
     * @throws Exception
     */
    public void testAll() throws Exception {
        testGetBuilding();
        testGetAllBuildings();
    }

    /* ************** */
    /* BUILDING TESTS */
    /* ************** */

    @Test
    public void testGetAllBuildings() throws Exception {
        List<Building> expected = BuildingTest.getQuickBuildingList();
        List<Building> test = facilitiesHelper.getAllBuildings();
        for(int i = 0; i < 2; i++) {
            assertTrue(expected.get(i).buildingEquals(test.get(i)));
        }
    }
    /**
     * Test if getBuilding correctly retrieves the building
     * with the specified id
     * @throws Exception
     */
    @Test
    public void testGetBuilding() throws Exception {
        Building b = BuildingTest.getQuickBuilding1();
        Building tB = facilitiesHelper.getBuilding(1);
        assertTrue(b.buildingEquals(tB));
    }

    @Test
    public void testAddNewBuilding() throws Exception {

    }

    @Test
    public void testRemoveBuilding() throws Exception {

    }

    /* *********** */
    /* FLOOR TESTS */
    /* *********** */
    @Test
    public void testGetAllFloors() throws Exception {
        List<Floor> expected = FloorTest.getQuickFloorList();
        List<Floor> test = facilitiesHelper.getAllFloors(1);
        for (int i = 0; i < test.size(); i++) {
            assertTrue(expected.get(i).floorEquals(test.get(i)));
        }
    }
    @Test
    public void testGetFloor() throws Exception {
        Floor expected = FloorTest.getQuickFloor1();
        Floor test = facilitiesHelper.getFloor(1);
        assertTrue(expected.floorEquals(test));
    }
    @Test
    public void testAddNewFloor() throws Exception {

    }

    @Test
    public void testRemoveFloor() throws Exception {

    }

    @Test
    public void testGetAllUnits() throws Exception {
        List<Unit> expected = UnitTest.getQuickList();
        List<Unit> actual = facilitiesHelper.getAllUnits(1);
        for(int i = 0; i < actual.size(); i++) {
            assertTrue(expected.get(i).unitEquals(actual.get(i)));
        }
    }

    @Test
    public void testGetUnit() throws Exception {
        Unit expected = UnitTest.getQuickUnit1();
        Unit actual = facilitiesHelper.getUnit(1);
        try {
            assertTrue(expected.unitEquals(actual));
        } catch(Exception e) {
            collector.addError(e);
        }
    }
    @Test
    public void testAddNewUnit() throws Exception {

    }

    @Test
    public void testRemoveUnit() throws Exception {

    }

    @Test
    public void testListBuildings() throws Exception {

    }

    @Test
    public void testListFloors() throws Exception {

    }

    @Test
    public void testListUnits() throws Exception {

    }
}