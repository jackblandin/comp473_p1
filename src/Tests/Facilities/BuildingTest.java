package Tests.Facilities;

import Facilities.Building;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BuildingTest extends TestCase {

    /**
     * Runs all tests in BuildingTest
     */
    public void testAll() throws Exception{
        testFullConstructor();
        testGetFieldTypes();
        testSetters();
    }

    /**
     * Test Constructor that requires 5 input parameters
     */
    @Test
    public void testFullConstructor() throws Exception{
        Building building = new Building("John Hancock", "875 North Michigan Avenue, Chicago, IL 60611", 4000, "office space", "JHC building info");
        assertEquals("John Hancock", building.getBuildingName());
        assertEquals("875 North Michigan Avenue, Chicago, IL 60611", building.getBuildingAddress());
        assertEquals(4000, building.getBuildingCapacity());
        assertEquals("office space", building.getBuildingDetail());
        assertEquals("JHC building info", building.getBuildingInformation());
    }

    /**
     * Test that a building returns a list of its field types
     * @throws Exception
     */
    @Test
    public void testGetFieldTypes() throws Exception {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("String");
        fieldTypes.add("String");
        fieldTypes.add("int");
        fieldTypes.add("String");
        assertEquals(fieldTypes, Building.getFieldTypes());
    }

    /**
     * Test Setters
     */
    @Test
    public void testSetters() {
        Building building = getQuickBuilding1();
        assertEquals("John Hancock", building.getBuildingName());
        assertEquals("875 North Michigan Avenue, Chicago, IL 60611", building.getBuildingAddress());
        assertEquals(5000, building.getBuildingCapacity());
        assertEquals("office space", building.getBuildingDetail());
        assertEquals("JHC building info", building.getBuildingInformation());
    }

    /**
     * helper function used for quickly creating a building
     * @return
     */
    public static Building getQuickBuilding1(){
        Building building = new Building();
        building.setBuildingId(1);
        building.setBuildingName("John Hancock");
        building.setBuildingAddress("875 North Michigan Avenue, Chicago, IL 60611");
        building.setBuildingCapacity(5000);
        building.setBuildingDetail("office space");
        building.setBuildingInformation("JHC building info");
        return building;
    }
    /**
     * helper function used for quickly creating a building
     * @return
     */
    public static Building getQuickBuilding2(){
        Building building = new Building();
        building.setBuildingId(2);
        building.setBuildingName("Willis Tower");
        building.setBuildingAddress("233 South Wacker Drive");
        building.setBuildingCapacity(5000);
        building.setBuildingDetail("office space");
        return building;
    }

    public static List<Building> getQuickBuildingList() {
        List<Building> bList = new ArrayList<Building>();
        bList.add(getQuickBuilding1());
        bList.add(getQuickBuilding2());
        return bList;
    }
}
