package Tests.Facilities;

import Facilities.Unit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnitTest {

    /**
     * runs all tests in UnitTest
     * @throws Exception
     */
    public void testAll() throws Exception {

    }
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSetUnitNumber() throws Exception {

    }

    @Test
    public void testSetUser() throws Exception {

    }

    @Test
    public void testGetUnitNumber() throws Exception {

    }

    @Test
    public void testGetUser() throws Exception {

    }

    /* **************** */
    /* HELPER FUNCTIONS */
    /* **************** */

    public static Unit getQuickUnit1() {
        Unit unit = new Unit();
        unit.setId(1);
        unit.setFloorId(1);
        unit.setUnitNumber(1);
        return unit;
    }
    public static Unit getQuickUnit2() {
        Unit unit = new Unit();
        unit.setId(2);
        unit.setFloorId(1);
        unit.setUnitNumber(2);
        return unit;
    }
    public static List<Unit> getQuickList() {
        List<Unit> uList = new ArrayList<Unit>();
        uList.add(getQuickUnit1());
        uList.add(getQuickUnit2());
        return uList;
    }
}