package Tests.DB;

import DB.DBHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DBHelperTest {

    /**
     * Runs all tests in DBHelperTest
     * @throws Exception
     */
    public void testAll() throws Exception {
        testSELECTQueryString();
        testGetConnection();
        testGetUPDATE();
    }

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test
    public void testSELECTQueryString() throws Exception {
        List<String> testFieldList = new ArrayList<String>();
        testFieldList.add("*");
        List<String> testInnerJoins = new ArrayList<String>();
        List<String> testConstraintList = new ArrayList<String>();
        String testSelect = DBHelper.SELECTQueryString(testFieldList, "tblBuildings", testConstraintList, "", testInnerJoins);
        String expected = "SELECT * FROM tblBuildings";
        assertEquals(expected, testSelect);

    }

    @Test
    public void testExecuteQuery() {

    }
    @Test
    public void testGetConnection() throws Exception {

    }

    @Test
    public void testINSERTQueryString() throws Exception {
        String expected = "INSERT INTO tblBuildings (name, address, capacity, detail) VALUES ('testName', 'testAddress', 100, 'testDetail')";
        List<String> testFields = new ArrayList<String>();
        testFields.add("name");
        testFields.add("address");
        testFields.add("capacity");
        testFields.add("detail");

        List<String> testValues = new ArrayList<String>();
        testValues.add("'testName'");
        testValues.add("'testAddress'");
        testValues.add("100");
        testValues.add("'testDetail'");

        String test = DBHelper.INSERTQueryString("tblBuildings", testFields, testValues);

        try {
            assertEquals(expected, test);
        } catch(Exception ex) {
            collector.addError(ex);
        }

    }

    @Test
    public void testGetUPDATE() throws Exception {
        String expected = "UPDATE tbl_userequests SET isused = true WHERE id = 11";
        List<String> testUpdates = new ArrayList<String>();
        String tblName = "tbl_userequests";
        testUpdates.add("isused = true");
        List<String> testConstraints = new ArrayList<String>();
        testConstraints.add("id = 11");
        String test = DBHelper.getUPDATE(tblName, testUpdates, testConstraints);

        try {
            assertEquals(expected, test);
        } catch(Exception ex) {
            collector.addError(ex);
        }
    }
}