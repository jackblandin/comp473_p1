package Tests.DB;

import DB.FacilitiesHelperDAO;
import Facilities.Building;
import Facilities.Floor;
import Facilities.Unit;
import Tests.Facilities.BuildingTest;
import Tests.Facilities.FloorTest;
import Tests.Facilities.UnitTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FacilitiesHelperDAOTest {

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    private FacilitiesHelperDAO facilitiesHelperDAO = new FacilitiesHelperDAO();

    public void testAll() throws Exception {
        testGetAllBuildings();
        testAddBuilding();
        System.out.println(collector);

    }

    /* ******************** */
    /* BUILDING DAO METHODS */
    /* ******************** */

    @Test
    public void testGetAllBuildings() throws Exception {
        List<Building> testBList = facilitiesHelperDAO.getAllBuildings();
        List<Building> expectedBList = new ArrayList<Building>();
        expectedBList.add(BuildingTest.getQuickBuilding1());
        expectedBList.add(BuildingTest.getQuickBuilding2());
        for(int i = 0; i < 2; i++) {
            assertTrue(expectedBList.get(i).buildingEquals(testBList.get(i)));
        }

    }

    @Test
    public void testGetBuilding() throws Exception {
        Building expectedBuilding = BuildingTest.getQuickBuilding1();
        Building testBuilding = facilitiesHelperDAO.getBuilding(1);
        assertTrue(expectedBuilding.buildingEquals(testBuilding));
    }

    @Test
    public void testAddBuilding() throws Exception {

    }

    @Test
    public void testRemoveBuilding() throws Exception {

    }

    /* ***************** */
    /* FLOOR DAO METHODS */
    /* ***************** */
    @Test
    public void testGetAllFloors() throws Exception {
        List<Floor> testFList = facilitiesHelperDAO.getAllFloors(1);
        List<Floor> expectedFList = new ArrayList<Floor>();
        expectedFList.add(FloorTest.getQuickFloor1());
        expectedFList.add(FloorTest.getQuickFloor2());
        for(int i = 0; i < testFList.size(); i++) {
            assertTrue(expectedFList.get(i).floorEquals(testFList.get(i)));
        }
    }

    @Test
    public void testGetFloor() throws Exception {
        Floor expectedFloor = FloorTest.getQuickFloor1();
        Floor testFloor = facilitiesHelperDAO.getFloor(1);
        assertTrue(expectedFloor.floorEquals(testFloor));
    }

    @Test
    public void testAddFloor() throws Exception {

    }

    @Test
    public void testRemoveFloor() throws Exception {

    }

    /* ********** */
    /* UNIT TESTS */
    /* ********** */

    @Test
    public void testGetAllUnits() throws Exception {
        List<Unit> test = facilitiesHelperDAO.getAllUnits(1);
        List<Unit> expected = UnitTest.getQuickList();

        for(int i = 0; i < test.size(); i++) {
            try {
                assertTrue(expected.equals(test));
            } catch(Throwable e) {
                collector.addError(e);
            }
        }
    }

    @Test
    public void testGetUnit() throws Exception {
        Unit expected = UnitTest.getQuickUnit1();
        Unit test = facilitiesHelperDAO.getUnit(1);
        try {
            assertTrue(expected.unitEquals(test));
        } catch (Throwable e) {
            collector.addError(e);
        }
    }

    @Test
    public void testAddUnit() throws Exception {

    }

    @Test
    public void testRemoveUnit() throws Exception {

    }

    @Test
    public void testExecuteQuery() throws Exception {

    }

    @Test
    public void testObjectToBuilding() throws Exception {

    }
}