package Tests.DB;

import DB.MaintenanceHelperDAO;
import FacilityMaintenance.MaintenanceRequest;
import Tests.FacilityMaintenance.MaintenanceRequestTest;
import main.Main;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class MaintenanceHelperDAOTest {

    private MaintenanceHelperDAO mHDAO = new MaintenanceHelperDAO();

    public void testAll() throws Exception {
        testGetAllMaintenanceRequests();
    }
    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetAllMaintenanceRequests() throws Exception {
        List<MaintenanceRequest>  expected = MaintenanceRequestTest.getQuickList();
        List<MaintenanceRequest> test = mHDAO.getAllMaintenanceRequests();
        for(int i = 0; i < test.size(); i++) {
            try {
                assertTrue(expected.get(i).facilityMaintenanceEquals(test.get(i)));
            } catch (Exception ex) {
                collector.addError(ex);
                System.out.println(ex.getMessage());
            }
        }

    }

    @Test
    public void testLlObjectToMaintenanceRequestList() throws Exception {

    }

    @Test
    public void testLlObjectToMaintenanceRequest() throws Exception {

    }

    @Test
    public void testGetAllMaintenanceRequests1() throws Exception {

    }

    @Test
    public void testGetAllMaintenance() throws Exception {

    }

    @Test
    public void testAddMaintenanceRequest() throws Exception {

    }

    @Test
    public void testDateToPostgreDate() throws Exception {
        Date testDate = new Date();
        String expected = "17 02 2015";
        String test = MaintenanceHelperDAO.dateToPostgreDate(testDate);
        try {
            assertEquals(expected, test);
        } catch (Exception ex) {
            collector.addError(ex);
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void testLlObjectToMaintenanceRequestList1() throws Exception {

    }

    @Test
    public void testLlObjectToMaintenanceRequest1() throws Exception {

    }

    @Test
    public void testLObjectToMaintenanceRequest() throws Exception {

    }
}