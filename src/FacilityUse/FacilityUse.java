package FacilityUse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dalestoutjr on 2/11/15.
 */
public class FacilityUse implements IFacilityUse {

    private UseHelper useHelper = new UseHelper();

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private int facilityId;
    private List<IUseRequest> facilityUseRequestList;
    private UseRequest currentUseRequest;

    /* *************** */
    /* GETTERS/SETTERS */
    /* *************** */

    public int getFacilityId() {return facilityId;}
    public void setFacilityId(int facilityId) {this.facilityId = facilityId;}
    public UseRequest getCurrentUseRequest() {return currentUseRequest;}
    public void setCurrentUseRequest(int useRequestId) {
        useHelper.setCurrentUseRequest(useRequestId, facilityId);
        this.currentUseRequest = currentUseRequest;
    }
    /**
     * get all use requests made for this facility
     * @param facilityId
     * @return
     */
    public List<IUseRequest> getUseRequestList(int facilityId) {
        List<IUseRequest> urList = useHelper.getAllUseRequests(facilityId, true);
        return urList;
    }

    public void setUseRequestList(List<IUseRequest> urList) {
        this.facilityUseRequestList = urList;
    }

    /**
     * get all actual usages for this facility
     * @return
     */
    public List<IUseRequest> getActualUsageList(){return this.facilityUseRequestList;}
    public boolean isInUseDuringInterval(Date dateFrom, Date dateTo){
//        Iterator iterator = facilityUseList.iterator();
//        while (iterator.hasNext()){
//            IFacilityUse tempFacilityUse = (IFacilityUse) iterator.next();
//            Date tempDateFrom = tempFacilityUse.getDateFrom();
//            Date tempDateTo = tempFacilityUse.getDateTo();
//            if (tempDateFrom.after(dateFrom) && tempDateTo.before(dateTo)){
//                return true;
//            }
//        }
        return false;

    }

    /* ******* */
    /* METHODS */
    /* ******* */

    public void addUseRequest(IUseRequest ur) {
        useHelper.addUseRequest(ur);
    }

    public void removeUseRequest(int useRequestId) {

    }
}
