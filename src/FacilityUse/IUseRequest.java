package FacilityUse;

import java.util.Date;
import java.util.List;

/**
 * Created by dalestoutjr on 2/6/15.
 */
public interface IUseRequest {

    public void setId(int id);
    public void setDateFrom(Date dateFrom);
    public void setDateTo(Date dateTo);
    public void setTenant(String facilityUser);
    public void setRent(double rent);
    public void setDescription(String facilityUseDescription);
    public void setInspection(String inspection);

    public int getId();
    public Date getDateFrom();
    public Date getDateTo();
    public String getTenant();
    public double getRent();
    public String getDescription();
    public String getInspection();
    public boolean getIsUsed();

    public String getAllInfo();
}
