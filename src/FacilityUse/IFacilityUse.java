package FacilityUse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dalestoutjr on 2/11/15.
 */
public interface IFacilityUse {

    public List<IUseRequest> getUseRequestList(int facilityId);
    public void setUseRequestList(List<IUseRequest> iurList);
    public boolean isInUseDuringInterval(Date dateFrom, Date dateTo);
    public void addUseRequest(IUseRequest facilityUse);
    public void removeUseRequest(int useRequestId);

}
