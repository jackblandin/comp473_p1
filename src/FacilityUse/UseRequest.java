package FacilityUse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dalestoutjr on 2/2/15.
 */
public class UseRequest implements IUseRequest {

    /* ********** */
    /* PROPERTIES */
    /* ********** */

    private int id;
    private int unitId;
    private Date dateFrom;
    private Date dateTo;
    private boolean isUsed;
    private String tenant;
    private double rent;
    private String inspection;
    private String description;
    /* ************ */
    /* CONSTRUCTORS */
    /* ************ */

    /**
     * EMPTY CONSTRUCTOR
     */
    public UseRequest() {
    }

    /**
     *
     * @param dateFrom start date for facility use
     * @param dateTo end date for facility use
     * @param isUsed current state of use
     * @param tenant user of facility
     * @param rent facility rent
     * @param description description of facility
     */
    public UseRequest(Date dateFrom, Date dateTo, boolean isUsed, String tenant, double rent, String description){
        this.id = -1;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.isUsed = isUsed;
        this.tenant = tenant;
        this.rent = rent;
        this.description = description;
        this.inspection = null;
    }

    /* ******* */
    /* GETTERS */
    /* ******* */

    public int getId(){
        return id;
    }
    public int getUnitId() { return unitId;}
    public Date getDateFrom() {
        return this.dateFrom;
    }
    public Date getDateTo() {
        return this.dateTo;
    }
    public String getTenant(){
        return this.tenant;
    }
    public double getRent(){
        return this.rent;
    }
    public String getUsed(){
        return this.description;
    }
    public boolean getIsUsed(){
        return this.isUsed;
    }
    public String getDescription() {return this.description;}
    public void setId(int id){
        this.id = id;
    }
    public void setDateFrom(Date dateFrom){this.dateFrom = dateFrom;}
    public void setDateTo(Date dateTo){this.dateTo = dateTo;}
    public void setUsed(Date used){
        this.dateTo = used;
    }
    public void setTenant(String tenant){
        this.tenant = tenant;
    }
    public void setRent(double rent){
        this.rent = rent;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setInspection(String inspection){
        this.inspection = inspection;
    }
    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }
    public void setIsUsed(boolean isUsed) {this.isUsed = isUsed;}
    public String getInspection() {
        return inspection;
    }

    public String getAllInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nid: " + Integer.toString(id));
        sb.append("\nunit id: " + Integer.toString(unitId));
        sb.append("\ndate from: " + dateFrom.toString());
        sb.append("\ndate to: " + dateTo.toString());
        sb.append("\ntenant: " + tenant);
        sb.append("\nrent: $" + Double.toString(rent));
        sb.append("\ninspection: " + inspection);
        sb.append("\ndescription: " + description);
        sb.append("\nis used: " + Boolean.toString(isUsed));
        return sb.toString();
    }
        /* HELPER METHODS */

    /**
     * get a lsit of FacilityUse field names
     * @return  list of names of FacilityUse fields
     */
    public static List<String> getFieldNames() {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("units_id");
        fieldNames.add("datefrom");
        fieldNames.add("dateto");
        fieldNames.add("tenant");
        fieldNames.add("rent");
        fieldNames.add("inspection");
        fieldNames.add("description");
        fieldNames.add("isused");
        return fieldNames;
    }

    /**
     * get a list of the types of fields that are in the database for FacilityUsage
     * @return list of types (as Strings)
     */
    public static List<String> getFieldTypes() {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("int");
        fieldTypes.add("Date");
        fieldTypes.add("Date");
        fieldTypes.add("String");
        fieldTypes.add("decimal");
        fieldTypes.add("String");
        fieldTypes.add("String");
        fieldTypes.add("boolean");
        return fieldTypes;
    }

    public boolean useRequestEquals(UseRequest ur) {
        if (ur == null) return false;
        if (ur == this) return true;
        if (ur.getId() != id) {
            return false;
        } else if (ur.getUnitId() != unitId) {
            return false;
        } else if (!ur.getDateFrom().equals(dateFrom)) {
            return false;
        } else if(!ur.getDateTo().equals(dateTo)) {
            return false;
        } else if (!ur.getUsed().equals(description)) {
            return false;
        } else if (!ur.getTenant().equals(tenant)) {
            return false;
        } else if (ur.getRent() != rent) {
            return false;
        } else if (!ur.getInspection().equals(inspection)) {
            return false;
        }
        else {
            return true;
        }
    }




}