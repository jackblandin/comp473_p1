package FacilityUse;

import DB.UseHelperDAO;

import java.util.List;

/**
 * Created by jackblandin on 2/16/15.
 */
public class UseHelper {

    private UseHelperDAO uHDAO= new UseHelperDAO();

    public List<IUseRequest> getAllUseRequests(int facilityId, boolean getOnlyUsed) {
        List<IUseRequest> urList = uHDAO.getAllUseRequests(facilityId, getOnlyUsed);
        return urList;
    }
    public void setCurrentUseRequest(int useRequestId, int facilityId) {
        uHDAO.setCurrentUseRequest(useRequestId, facilityId);
    }
    public void addUseRequest(IUseRequest ur) {

    }
    public void removeUseRequest(int useRequestId) {

    }
    public void vacateUnit(int unitId) {
        uHDAO.vacateUnit(unitId);
    }
    public double calculateUsageRate(int unitId) {
        double usageRate = uHDAO.calculateUsageRate(unitId);
        return usageRate;
    }
}
