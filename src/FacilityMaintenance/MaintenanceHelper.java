package FacilityMaintenance;

import DB.MaintenanceHelperDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jackblandin on 2/15/15.
 */
public class MaintenanceHelper {

    private MaintenanceHelperDAO maintenanceHelperDAO = new MaintenanceHelperDAO();

    public List<MaintenanceRequest> getAllMaintenanceRequests() {
        List<MaintenanceRequest> maintenanceRequests = maintenanceHelperDAO.getAllMaintenanceRequests();
        return maintenanceRequests;
    }

    public List<MaintenanceRequest> getAllMaintenance(int facilityId) {
        List<MaintenanceRequest> mrList = maintenanceHelperDAO.getAllMaintenance(facilityId);
        return mrList;
    }

    public void addMaintenanceRequest(MaintenanceRequest mr) {
        maintenanceHelperDAO.addMaintenanceRequest(mr);
    }

    public void scheduleMaintenanceRequest(int mrId, Date dateStart, Date dateEnd) {
        maintenanceHelperDAO.scheduleMaintenanceRequest(mrId, dateStart, dateEnd);
    }

}
