package FacilityMaintenance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dalestoutjr on 2/7/15.
 */
public class MaintenanceRequest implements IMaintenanceRequest {

    private int id;
    private int unitsId;
    private String name;
    private Date dateRequested;
    private Date dateCompleted;
    boolean isComplete;
    private String description;
    private double scope;
    private Date dateStart;
    private Date dateEnd;
    
    public MaintenanceRequest(){}
    
    public MaintenanceRequest(Date dateRequested, String name, String description, double scope){
    	this.name = name;
        this.dateRequested = dateRequested;
    	this.dateCompleted = null;
    	this.isComplete = false;
    	this.description = description;
        this.scope = scope;
    }

    /* GETTERS/SETTERS */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUnitsId() {
        return unitsId;
    }

    public void setUnitsId(int unitsId) {
        this.unitsId = unitsId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isMaintenanceCompleted) {
        this.isComplete = isMaintenanceCompleted;
    }

    public void setScope(double scope) {
        this.scope = scope;
    }
    public void setDateRequested(Date dateRequested){
    	this.dateRequested = dateRequested;
    }
    public void setDateCompleted(Date dateCompleted){
    	this.dateCompleted = dateCompleted;
    }
    public void setIsMaintenanceCompleted(boolean isMaintenanceCompleted){
    	this.isComplete = isMaintenanceCompleted;
    }
    public void setDescription(String description){
    	this.description = description;
    }
    public Date getDateRequested(){
    	return this.dateRequested;
    }
    public Date getDateCompleted(){
    	return this.dateCompleted;
    }
    public boolean getIsMaintenanceCompleted(){
    	return this.isComplete;
    }
    public String getDescription(){
    	return this.description;
    }
    public double getScope(){
        return this.scope;
    }
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */

    public String getAllInfo() {
        StringBuilder info = new StringBuilder();
        info.append("id: " + Integer.toString(id));
        info.append("\nbuildingId: " + Integer.toString(unitsId));
        info.append("\nname: " + name);
        String dateCompleted;
        try {
            dateCompleted = getDateCompleted().toString();
        } catch(Exception ex) {
            dateCompleted = "N/A";
        }
        info.append("\ndate requested: " + getDateRequested().toString());
        info.append("\ndate completed: " + dateCompleted);
        info.append("\ndescription: " + getDescription());
        info.append("\nscope: " + Double.toString(getScope()));
        String dateStart;
        String dateEnd;
        try {
            dateStart = getDateStart().toString();
            dateEnd = getDateEnd().toString();
        } catch (Exception ex) {
            dateStart = "N/A";
            dateEnd = "N/A";
        }
        info.append("\ndate start: " + dateStart);
        info.append("\ndate end: " + dateEnd);
        return info.toString();
    }

    public static List<String> getFieldNames() {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("dateto");
        fieldNames.add("datefrom");
        fieldNames.add("name");
        fieldNames.add("description");
        fieldNames.add("scope");
        fieldNames.add("units_id");
        fieldNames.add("datestart");
        fieldNames.add("dateend");
        return fieldNames;
    }

    public static List<String> getFieldTypes() {
        List<String> fieldTypes = new ArrayList<String>();
        fieldTypes.add("int");
        fieldTypes.add("Date");
        fieldTypes.add("Date");
        fieldTypes.add("String");
        fieldTypes.add("String");
        fieldTypes.add("decimal");
        fieldTypes.add("int");
        fieldTypes.add("Date");
        fieldTypes.add("Date");
        return fieldTypes;
    }


    public boolean facilityMaintenanceEquals(MaintenanceRequest mr) {
        if (mr == null) return false;
        if (mr == this) return true;
        if (mr.getId() !=  id) {
            return false;
        } else if(mr.getUnitsId() != unitsId) {
            return false;
        } else if(!calendarDayEquals(mr.getDateRequested(), dateRequested)) {
            return false;
        } else if(!mr.getDateCompleted().equals(dateCompleted)
                && !calendarDayEquals(mr.getDateCompleted(), dateCompleted)) {
            return false;
        } else if(mr.getScope() != scope) {
            return false;
        } else if(!mr.getDescription().equals(description)) {
            return false;
        } else if(!mr.getName().equals(name)) {
            return false;
        } else {
            return true;
        }
    }
    public static boolean calendarDayEquals(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return sameDay;
    }
}
