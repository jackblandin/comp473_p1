package FacilityMaintenance;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dalestoutjr on 2/6/15.
 */
public interface IFacilityMaintenance {

    public void setFacilityMaintenanceId(int Id);
    public void setMaintenanceRequest(MaintenanceRequest maintenanceRequest);

    public int getFacilityMaintenanceId();
    public IMaintenanceRequest getMaintenanceRequest();
    public ArrayList getMaintenanceLogList();

//    public void createMaintenanceRequest(Date dateRequested, String maintenanceDescription);
    public void listFacilityProblems();
    public void listMaintRequests();
    public void listMaintenance();
    public void calcMaintenanceCostForFacility();
    public void calcDownTimeForFacility();

}
