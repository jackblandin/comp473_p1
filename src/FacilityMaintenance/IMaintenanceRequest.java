package FacilityMaintenance;

import java.util.Date;

/**
 * Created by dalestoutjr on 2/7/15.
 */
public interface IMaintenanceRequest {
	
	 public void setDateRequested(Date dateRequested);
	 public void setDateCompleted(Date dateCompleted);
	 public void setIsMaintenanceCompleted(boolean isMaintenanceCompleted);
	 public void setDescription(String maintenanceDescription);
	 
	 public Date getDateRequested();
	 public Date getDateCompleted();
	 public boolean getIsMaintenanceCompleted();
	 public String getDescription();
    public double getScope();

}
