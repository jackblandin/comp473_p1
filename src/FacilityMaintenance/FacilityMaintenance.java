package FacilityMaintenance;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by dalestoutjr on 2/2/15.
 */
public class FacilityMaintenance implements IFacilityMaintenance {

    private MaintenanceHelper mHelper = new MaintenanceHelper();
    private int facilityMaintenanceId;
    private IMaintenanceRequest maintenanceRequest;
    private ArrayList<IMaintenanceRequest> maintenanceLogList;

    /**
     * DEFAULT CONSTRUCTOR
     */
    public FacilityMaintenance(){}

    public FacilityMaintenance(int FacilityMaintenanceId, MaintenanceRequest maintenanceRequest){
        this.facilityMaintenanceId = FacilityMaintenanceId;
        this.maintenanceRequest = maintenanceRequest;
        this.maintenanceLogList = new ArrayList<IMaintenanceRequest>();
    }
    /**
     * SETTER
     * @param facilityMaintenanceId
     */
    public void setFacilityMaintenanceId(int facilityMaintenanceId){
        this.facilityMaintenanceId = facilityMaintenanceId;
    }

    /**
     * SETTER
     * @param maintenanceRequest
     */
    public void setMaintenanceRequest(MaintenanceRequest maintenanceRequest){
        this.maintenanceRequest = maintenanceRequest;
    }

    /**
     * GETTER
     * @return facilityMaintenanceId
     */
    public int getFacilityMaintenanceId(){
        return this.facilityMaintenanceId;
    }

    /**
     * GETTER
     * @return maintenanceRequest
     */
    public IMaintenanceRequest getMaintenanceRequest(){
        return this.maintenanceRequest;
    }

    /**
     * GETTER
     * @return maintenanceLogList
     */
    public ArrayList getMaintenanceLogList(){
        return this.maintenanceLogList;
    }

    public void createMaintenanceRequest(MaintenanceRequest mr){
    }

    /**
     * METHOD
     */
    public void listFacilityProblems(){
        Iterator iterator = maintenanceLogList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }
    }

    /**
     * METHOD
     */
    public void listMaintRequests(){
    }

    /**
     * METHOD
     */
    public void listMaintenance(){
        Iterator iterator = maintenanceLogList.iterator();
        IMaintenanceRequest tempMaintReq;
        while (iterator.hasNext()){
            tempMaintReq = (MaintenanceRequest) iterator.next();
            if(tempMaintReq.getIsMaintenanceCompleted()) {
                System.out.println(tempMaintReq.toString());
            }
        }
    }

    /**
     * METHOD
     */
    public void calcMaintenanceCostForFacility(){
        double total = 0;
        Iterator iterator = maintenanceLogList.iterator();
        IMaintenanceRequest tempMaintReq;
        while (iterator.hasNext()){
            tempMaintReq = (MaintenanceRequest) iterator.next();
            total =+ tempMaintReq.getScope();
            }
        System.out.println(total);
    }

    public void calcDownTimeForFacility(){
        Iterator iterator = maintenanceLogList.iterator();
        IMaintenanceRequest tempMaintReq;
        while (iterator.hasNext()){
            tempMaintReq = (MaintenanceRequest) iterator.next();
            if(tempMaintReq.getIsMaintenanceCompleted()) {
               int numberOfDays = (tempMaintReq.getDateRequested()).compareTo(tempMaintReq.getDateCompleted());
               System.out.println(numberOfDays);
            }
        }
    }








}
