/**
 * tbl_buildings
 */
CREATE TABLE public.tbl_buildings
(
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    capacity INT NOT NULL,
    detail VARCHAR(50) NOT NULL
);
ALTER TABLE public.tbl_buildings
 ADD CONSTRAINT unique_id UNIQUE (id);
/**
 * tbl_floors
 */
CREATE TABLE public.tbl_floors
(
  id SERIAL PRIMARY KEY NOT NULL,
  level INT NOT NULL,
  building_id INT NOT NULL
);
ALTER TABLE public.tbl_floors
 ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE public.tbl_floors
 ADD FOREIGN KEY (building_id) REFERENCES tbl_buildings (id);
/**
 * tbl_units
 */
CREATE TABLE public.tbl_units
(
  id SERIAL PRIMARY KEY NOT NULL,
  floors_id INT NOT NULL,
  number INT NOT NULL
);
ALTER TABLE public.tbl_units
 ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE public.tbl_units
 ADD FOREIGN KEY (floors_id) REFERENCES tbl_floors (id);
/**
 * tbl_maintenancerequests
 */
CREATE TABLE public.tbl_maintenancerequests
(
  id SERIAL PRIMARY KEY NOT NULL,
  dateto DATE,
  datefrom DATE NOT NULL,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) NOT NULL,
  scope DECIMAL NOT NULL,
  units_id INT NOT NULL,
  datestart DATE,
  dateend DATE
);
ALTER TABLE public.tbl_maintenancerequests
ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE public.tbl_maintenancerequests
ADD FOREIGN KEY (units_id) REFERENCES tbl_units (id);
/**
 * tbl_userequests
 */
CREATE TABLE public.tbl_userequests
(
  id SERIAL PRIMARY KEY NOT NULL,
  units_id INT NOT NULL,
  datefrom DATE NOT NULL,
  dateto DATE NOT NULL,
  tenant VARCHAR(50) NOT NULL,
  rent DECIMAL NOT NULL,
  inspection VARCHAR(50) NOT NULL,
  description VARCHAR(255) NOT NULL,
  isused BOOLEAN DEFAULT FALSE
);
ALTER TABLE public.tbl_userequests
ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE public.tbl_userequests
ADD FOREIGN KEY (units_id) REFERENCES tbl_units (id);
f1
/*
 * populate table data
 */
INSERT INTO public.tbl_buildings (id, name, address, capacity, detail) VALUES (1, 'John Hancock', ' 875 North Michigan Avenue, Chicago, IL 60611', 5000, 'office space');
INSERT INTO public.tbl_buildings (id, name, address, capacity, detail) VALUES (2, 'Willis Tower', '233 South Wacker Drive', 5000, 'office space');
INSERT INTO public.tbl_buildings (id, name, address, capacity, detail) VALUES (3, 'Trum Tower', '401 N Wabash Ave', 2000, 'hotel');
INSERT INTO public.tbl_floors (id, level, building_id) VALUES (1, 1, 1);
INSERT INTO public.tbl_units (id, floors_id, number) VALUES (1, 1, 1);
INSERT INTO public.tbl_units (id, floors_id, number) VALUES (2, 1, 2);
INSERT INTO public.tbl_userequests (id, units_id, datefrom, dateto, tenant, rent, inspection, description, isused) VALUES (1, 1, '2015-02-02', '2015-02-12', 'Jack Blandin', 300, 'sample inspection', 'this is the description for a sample inspection', false);
INSERT INTO public.tbl_userequests (id, units_id, datefrom, dateto, tenant, rent, inspection, description, isused) VALUES (2, 1, '2014-02-02', '2014-02-12', 'Michael Jordan', 300, 'inspecting kitchen', 'sample description', true);
INSERT INTO public.tbl_userequests (id, units_id, datefrom, dateto, tenant, rent, inspection, description, isused) VALUES (3, 1, '2014-02-25', '2015-02-17', 'Derek Rose', 1000, 'inspection sample', 'sample description', true);
INSERT INTO public.tbl_maintenancerequests (id, dateto, datefrom, name, description, scope, units_id, datestart, dateend) VALUES (3, '2015-02-10', '2014-02-20', 'clean bathroom tile', 'clean bathroom floor', 200, 2, null, null);
INSERT INTO public.tbl_maintenancerequests (id, dateto, datefrom, name, description, scope, units_id, datestart, dateend) VALUES (1, '2015-02-15', '2015-02-15', 'clean floors', 'mop all floors', 100, 1, null, null);
INSERT INTO public.tbl_maintenancerequests (id, dateto, datefrom, name, description, scope, units_id, datestart, dateend) VALUES (4, '2012-02-10', '2012-02-20', 'clean kitchen tile', 'clean kitchen floor', 400, 2, null, null);
INSERT INTO public.tbl_maintenancerequests (id, dateto, datefrom, name, description, scope, units_id, datestart, dateend) VALUES (5, '1984-04-12', '1981-11-01', 'clean kitchen tile', 'clean kitchen floor', 400, 2, null, null);
INSERT INTO public.tbl_maintenancerequests (id, dateto, datefrom, name, description, scope, units_id, datestart, dateend) VALUES (6, '2015-01-25', '1990-01-12', 'test2', 'testing add new maintenance request', 200, 1, '2000-01-01', '2001-01-01');
